#ifndef _DISP_DIRECT_H_
#define _DISP_DIRECT_H_

#define DD_COLOR_EMPTY             0x00
#define DD_COLOR_RED               0x01
#define DD_COLOR_GREEN             0x02
#define DD_COLOR_BLUE              0x03
#define DD_COLOR_CYAN              0x04
#define DD_COLOR_MAGENTA           0x05
#define DD_COLOR_YELLOW            0x06
#define DD_COLOR_ORANGE            0x07
#define DD_COLOR_TRANSPARENT_BLACK 0x08
#define DD_COLOR_BLACK             0x09
#define DD_COLOR_GRAY              0x0A
#define DD_COLOR_WHITE             0x0F

void disp_set_pixel(uint32_t x, uint32_t y, uint32_t color);
void disp_init();

void print_line(uint32_t color, uint32_t scale, char *txt);

#endif
