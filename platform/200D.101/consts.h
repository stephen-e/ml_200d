/*
 *  200D 1.0.1 consts
 */

#define CARD_LED_ADDRESS            0xD208016C   /* WLAN LED at 0xD2080190 */
#define LEDON                       0x20D0002
#define LEDOFF                      0x20C0003

// SJE needed to build CONFIG_HELLO_WORLD
#define HIJACK_TASK_ADDR            0x00001028
#define GMT_FUNCTABLE               0xE0805F20 // fairly sure, untested
#define GMT_NFUNCS                  0x7 // fairly sure, untested

// fairly sure, untested:
#define LVAE_STRUCT 0x86308 // eg see 0xe027d866 for Tv, Av, ISO found via string search on EP_SetControlParam
#define CONTROL_BV      (*(uint16_t*)(LVAE_STRUCT+0x28)) // EP_SetControlBv, SJE: if sequential as in 50D, then 0x28
                                                         // CtrlBv string maybe better hits?
#define CONTROL_BV_TV   (*(uint16_t*)(LVAE_STRUCT+0x2a))
#define CONTROL_BV_AV   (*(uint16_t*)(LVAE_STRUCT+0x2c))
#define CONTROL_BV_ISO  (*(uint16_t*)(LVAE_STRUCT+0x2e))
#define CONTROL_BV_ZERO (*(uint16_t*)(LVAE_STRUCT+0x30)) // SJE strings ref AccumH, don't know where BV_ZERO comes from
#define LVAE_ISO_SPEED  (*(uint8_t* )(LVAE_STRUCT+0x0))  // offset 0x0; at 3 it changes iso very slowly
                                                         // SJE: assuming the 0 offset is correct, not sure on this one.
                                                         // But see e027ed10, where r4 <- 0x86308, then [r4] <- 0
#define LVAE_ISO_MIN    (*(uint8_t* )(LVAE_STRUCT+0xXX)) // string: ISOMin:%d, SJE maybe 0x1a via e027e828 (if so, uint16_t)
#define LVAE_ISO_HIS    (*(uint8_t* )(LVAE_STRUCT+0xXX)) // 10DFC 88 ISO LIMIT
#define LVAE_DISP_GAIN  (*(uint16_t*)(LVAE_STRUCT+0x44)) // lvae_setdispgain
#define LVAE_MOV_M_CTRL (*(uint8_t* )(LVAE_STRUCT+0xXX)) // lvae_setmoviemanualcontrol, possibly EP_SetMovieManualExp below?
// others found but maybe not needed:
/*
#define CONTROL_FLICKER_MODE (*(uint32_t*)(LVAE_STRUCT+0x38)) // EP_SetFlickerMode
#define CONTROL_INTERPOLATE (*(uint32_t*)(LVAE_STRUCT+0x34)) // EP_SetInterpolateControl
#define CONTROL_ACCUM_H (*(uint16_t*)(LVAE_STRUCT+0x30)) // EP_SetControlAccumH - this is apparently CONTROL_BV_ZERO
#define CONTROL_MOVIE_MANUAL_E (*(uint16_t*)(LVAE_STRUCT+0x24)) // EP_SetMovieManualExposureMode
#define CONTROL_MANUAL_EXPOSURE (*(uint32_t*)(LVAE_STRUCT+0x20)) // EP_SetManualExposureMode
#define CONTROL_INITIAL_BV (*(unsure, strh.w*)(LVAE_STRUCT+0x4c)) // EP_SetInitialBv
*/

// This one is weird.  There are two very similar functions, e02f2fda e02f3a9a,
// each has an early local var pulling from either 0x123e0 or 0123ac, and each later calling
// the same function that returns 0xaf2d0.  Some kind of double buffering?  50D looks quite different.
// I can't see why two functions is the best way of doing it (or, one for each core??)
// The closest 50D match I could find has the same PAL / NTSC / HDMI style strings.
#define LV_STRUCT_PTR 0xaf2d0
// below definitely wrong, just copied from 50D
#define FRAME_SHUTTER *(uint8_t*)(MEM(LV_STRUCT_PTR) + 0x56)
#define FRAME_APERTURE *(uint8_t*)(MEM(LV_STRUCT_PTR) + 0x57)
#define FRAME_ISO *(uint16_t*)(MEM(LV_STRUCT_PTR) + 0x58)
#define FRAME_SHUTTER_TIMER *(uint16_t*)(MEM(LV_STRUCT_PTR) + 0x5c)
#define FRAME_BV ((int)FRAME_SHUTTER + (int)FRAME_APERTURE - (int)FRAME_ISO)

// everything SJE below here is unsure:
#define AUDIO_MONITORING_HEADPHONES_CONNECTED 0
#define MIN_MSLEEP 11
#define FOCUS_CONFIRMATION (*(int*)0x4444) // wrong, focusinfo looks really different 50D -> 200D
#define DISPLAY_SENSOR_POWERED (*(int *))(0xc678) // c638 looks like base of struct, not sure on the fields.  from 0xe014969e
#define DISPLAY_IS_ON               (*(int*)0x387C) // wrong 0x1 // assume always on
#define INFO_BTN_NAME "INFO"
#define Q_BTN_NAME "FUNC"
#define ARROW_MODE_TOGGLE_KEY "FUNC"
//#define HIJACK_INSTR_BSS_END        0x00A96000 // unsure (likely wrong and uneeded once boot-d78 style is ported
#define AF_BTN_HALFSHUTTER 0
// assume menu always, for now
#define CURRENT_GUI_MODE (*(int*)0x387C) // 50d, probably wrong
#define CURRENT_GUI_MODE_2 2
// these are all copied from 50D
#define GUIMODE_ML_MENU 2
#define GUIMODE_WB 5
#define GUIMODE_FOCUS_MODE 9
#define GUIMODE_DRIVE_MODE 8
#define GUIMODE_PICTURE_STYLE 4
#define GUIMODE_PLAY 1
#define GUIMODE_MENU 2
#define GUIMODE_Q_UNAVI 0x18
#define GUIMODE_FLASH_AE 0x22
#define GUIMODE_PICQ 6

#define WINSYS_BMP_DIRTY_BIT_NEG MEM(0x4444+0x30) // wrong, no idea

#define NUM_PICSTYLES 10 // guess, but seems to be always 9 for old cams, 10 for new


#define BFNT_CHAR_CODES    0x00000000 // These are wrong, 200D uses opentype, not bitmap fonts.
#define BFNT_BITMAP_OFFSET 0x00000000 // However, I am using 0x0 to trigger a workaround.
#define BFNT_BITMAP_DATA   0x00000000 // This makes rbf fonts work, but you can't use
                                      // in-built fonts for early debugging.

#define PLAY_MODE (gui_state == GUISTATE_PLAYMENU && CURRENT_GUI_MODE == GUIMODE_PLAY)
#define MENU_MODE (gui_state == GUISTATE_PLAYMENU && CURRENT_GUI_MODE == GUIMODE_MENU)

// all these MVR ones are junk, don't try and record video and they probably don't get used?
#define MVR_190_STRUCT (*(void**)0x1ed8) // look in MVR_Initialize for AllocateMemory call;
                                         // decompile it and see where ret_AllocateMemory is stored.
#define div_maybe(a,b) ((a)/(b))
// see mvrGetBufferUsage, which is not really safe to call => err70
// macros copied from arm-console
#define MVR_BUFFER_USAGE 0 /* obviously wrong, don't try and record video
       // div_maybe(-100*MEM(236 + MVR_190_STRUCT) - \
       // 100*MEM(244 + MVR_190_STRUCT) - 100*MEM(384 + MVR_190_STRUCT) - \
       // 100*MEM(392 + MVR_190_STRUCT) + 100*MEM(240 + MVR_190_STRUCT) + \
       // 100*MEM(248 + MVR_190_STRUCT), \
       // - MEM(236 + MVR_190_STRUCT) - MEM(244 + MVR_190_STRUCT) + \
       // MEM(240 + MVR_190_STRUCT) +  MEM(248 + MVR_190_STRUCT)) */
#define MVR_FRAME_NUMBER (*(int*)(220 + MVR_190_STRUCT))
//#define MVR_LAST_FRAME_SIZE (*(int*)(512 + MVR_752_STRUCT))
#define MVR_BYTES_WRITTEN MEM((212 + MVR_190_STRUCT))

#define LV_BOTTOM_BAR_DISPLAYED 0x0 // wrong, fake bool

// this block all copied from 50D, and probably wrong, though likely safe
#define FASTEST_SHUTTER_SPEED_RAW 160
#define MAX_AE_EV 2
#define FLASH_MAX_EV 3
#define FLASH_MIN_EV -10
#define COLOR_FG_NONLV 80
#define AF_BTN_HALFSHUTTER 0
#define AF_BTN_STAR 2

// all these YUV ones are junk, just copied from 50D
// 720x480, changes when external monitor is connected
#define YUV422_LV_BUFFER_1 0x41B00000
#define YUV422_LV_BUFFER_2 0x5C000000
#define YUV422_LV_BUFFER_3 0x5F600000
#define YUV422_LV_PITCH 1440
//~ #define YUV422_LV_PITCH_RCA 1080
//~ #define YUV422_LV_PITCH_HDMI 3840
//~ #define YUV422_LV_HEIGHT 480
//~ #define YUV422_LV_HEIGHT_RCA 540
//~ #define YUV422_LV_HEIGHT_HDMI 1080

// not 100% sure, copied from 550D/5D2/500D
#define REG_EDMAC_WRITE_LV_ADDR 0xc0f26208 // SDRAM address of LV buffer (aka VRAM)
#define REG_EDMAC_WRITE_HD_ADDR 0xc0f04008 // SDRAM address of HD buffer (aka YUV)

#define YUV422_LV_BUFFER_DISPLAY_ADDR (*(uint32_t*)0x28f8)
#define YUV422_HD_BUFFER_DMA_ADDR (shamem_read(REG_EDMAC_WRITE_HD_ADDR))

#define YUV422_HD_BUFFER_1 0x44000080
#define YUV422_HD_BUFFER_2 0x46000080
#define YUV422_HD_BUFFER_3 0x48000080
#define YUV422_HD_BUFFER_4 0x4e000080
#define YUV422_HD_BUFFER_5 0x50000080

#define DRYOS_ASSERT_HANDLER 0x4000 // Used early in a function I've named debug_assert_maybe
#define AE_STATE (*(int8_t*)(0xFB30 + 0x1C)) 
#define AE_VALUE (*(int8_t*)(0xFB30 + 0x1D))
#define ARMLIB_OVERFLOWING_BUFFER 0x1e948 // in AJ_armlib_setup_related3
#define BULB_EXPOSURE_CORRECTION 150 // min value for which bulb exif is OK [not tested]
#define BULB_MIN_EXPOSURE 500
#define CACHE_HACK_FLUSH_RATE_SLAVE 0xFF84D358
#define CANON_SHUTTER_RATING 100000
#define DIALOG_MnCardFormatBegin (0x1e704+4) // ret_CreateDialogBox(...DlgMnCardFormatBegin_handler...) is stored there
#define DIALOG_MnCardFormatExecute (0x1E7B8+4) // similar
#define DISPLAY_CLOCK_POS_X 200
#define DISPLAY_CLOCK_POS_Y 410
#define DISPLAY_TRAP_FOCUS_MSG_BLANK "     \n     "
#define DISPLAY_TRAP_FOCUS_MSG       "TRAP \nFOCUS"
#define DISPLAY_TRAP_FOCUS_POS_X 410
#define DISPLAY_TRAP_FOCUS_POS_Y 330
#define EFIC_CELSIUS ((int)efic_temp - 128)
#define FORMAT_BTN BGMT_FUNC
#define FORMAT_BTN_NAME "[FUNC]"
#define FORMAT_STR_LOC 6
#define FRAME_SHUTTER_BLANKING_NOZOOM (*(uint16_t*)0x404B5A30) // ADTG register 1061
#define FRAME_SHUTTER_BLANKING_READ   (lv_dispsize > 1 ? FRAME_SHUTTER_BLANKING_NOZOOM : FRAME_SHUTTER_BLANKING_ZOOM) /* when reading, use the other mode, as it contains the original value (not overriden) */
#define FRAME_SHUTTER_BLANKING_ZOOM   (*(uint16_t*)0x404B5A2C) // ADTG register 105F
#define GUIMODE_MOVIE_ENSURE_A_LENS_IS_ATTACHED (CURRENT_GUI_MODE == 0x1B) //Sure
#define GUIMODE_MOVIE_PRESS_LV_TO_RESUME (CURRENT_GUI_MODE == 0x1C) //Not Sure
#define HOTPLUG_VIDEO_OUT_PROP_DELIVER_ADDR 0x1af8 // this prop_deliver performs the action for Video Connect and Video Disconnect
#define HOTPLUG_VIDEO_OUT_STATUS_ADDR 0x1b24 // passed as 2nd arg to prop_deliver; 1 = display connected, 0 = not, other values disable this event (trick)
#define IMGPLAY_ZOOM_LEVEL_ADDR (0xFA14+12) // dec GuiImageZoomDown and look for a negative counter
#define IMGPLAY_ZOOM_LEVEL_MAX 14
#define IMGPLAY_ZOOM_POS_DELTA_X (0x380 - 0x252)
#define IMGPLAY_ZOOM_POS_DELTA_Y (0x18c - 0xd8)
#define IMGPLAY_ZOOM_POS_X_CENTER 0x252
#define IMGPLAY_ZOOM_POS_X MEM(0x36360) // Zoom CentrePos
#define IMGPLAY_ZOOM_POS_Y_CENTER 0x18c
#define IMGPLAY_ZOOM_POS_Y MEM(0x36364)
#define ISO_ADJUSTMENT_ACTIVE ((*(int*)0x6A50) == 0xF)
#define MENU_DISP_ISO_POS_X 500
#define MENU_DISP_ISO_POS_Y 27
#define MOV_GOP_OPT_NUM_PARAMS 0
#define MOV_GOP_OPT_STEP 2
#define MOV_OPT_NUM_PARAMS 2
#define MOV_OPT_STEP 2
#define MOV_RES_AND_FPS_COMBINATIONS 2
#define SHOOTING_MODE (*(int*)0x313C)
#define ROM_ALLOCMEM_END 0xFF813230
#define ROM_ALLOCMEM_INIT 0xFF813238 
#define ROM_B_CREATETASK_MAIN 0xFF811E30
#define ROM_CREATETASK_MAIN_END 0xFF8134A0
#define ROM_CREATETASK_MAIN_START 0xFF813210
#define ROM_ITASK_END  0xFF81C8C4
#define ROM_ITASK_START 0xFF811DBC
#define ASIF_MAX_VOL 5
#define HALFSHUTTER_PRESSED 0
// SJE end of needed to build section


#define HIJACK_FIXBR_DCACHE_CLN_1   0xE0040068   /* first call to dcache_clean, before cstart */
#define HIJACK_FIXBR_ICACHE_INV_1   0xE0040072   /* first call to icache_invalidate, before cstart */
#define HIJACK_FIXBR_DCACHE_CLN_2   0xE00400A0   /* second call to dcache_clean, before cstart */
#define HIJACK_FIXBR_ICACHE_INV_2   0xE00400AA   /* second call to icache_invalidate, before cstart */
#define HIJACK_INSTR_BL_CSTART      0xE00400C0   /* easier to fix up here, rather than at E0040034 */
#define HIJACK_INSTR_HEAP_SIZE      0xE00401D0   /* easier to patch the size; start address is computed */
#define HIJACK_FIXBR_BZERO32        0xE004014A   /* called from cstart */
#define HIJACK_FIXBR_CREATE_ITASK   0xE00401AC   /* called from cstart */
#define HIJACK_INSTR_MY_ITASK       0xE00401DC   /* address of init_task passed to create_init_task */

#define MALLOC_STRUCT 0x1F1C8 // SJE unsure if needed?
#define MALLOC_FREE_MEMORY 0
