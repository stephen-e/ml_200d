/*
    SJE, jump hook code for debugging.  QEMU doesn't work for 200D yet
    and I want a way to inspect function state before e.g., crashes.

    So, patch in jump hooks before the point of interest, log stuff,
    then return to where we were.  We have a different asm block
    for each address we want to hook.  Ugly, saves us having
    to handle pushing the address before jmp, we can hard-code it.

    Note that we might be in Thumb mode or not when we jump,
    you have to write the hook block knowing what mode you came from.

    Some stuff based on reboot.c
*/
#include "internals.h"
#include "bmp.h"
#include "log-d678.h"

// reboot.c says they need this ASM block to be the first thing in the file...
// It's not explained why, so I'll copy it just in case.
#pragma GCC optimize ("-fno-reorder-functions")

#if !(defined(CONFIG_DIGIC_VII) || defined(CONFIG_DIGIC_VIII))
#   error "Refusing to build jump_hooks.c on Digic <= 7.\n  Code is too hacky and dependent on mixed Thumb/ARM code."
#endif

// To be more general (and safer!) we should probably deal with interrupts
// and caching.  See code in digic6-dumper branch, log-d678.c,
// "override Canon's DebugMsg".
//
// This is intended as a quick hack for debugging to understand a crash,
// so I'm not concerned about stability of this code.

// function is not designed to be called, instead the intention
// is to ldr pc direct to the hook label inside it.
void _jump_hook_dummy(void)
{
    asm(
        ".text\n"
        ".globl _hook_0xdf00a434\n" // This function returns a value that triggers an assert
        "_hook_0xdf00a434:\n"
        ".thumb\n" // assume Thumb, don't hook from non-Thumb context!
        //".extern bmp_printf_auto\n"

        // Push all normal regs.
        // Apparently, some ultra-optimised functions will
        // use the specials, if you use one of these inside here,
        // bad things will happen to you.
//        "push   {r0-r11}\n"
        // SJE can I just push all regs apart from PC?

    // On dual-core cams, guard against our hooks running on both CPUs.
    // Don't know if this is needed in hooking context, just a precaution.
    #if defined(CONFIG_DIGIC_VII) || defined(CONFIG_DIGIC_VIII)
        "mrc    p15, 0, r8, c0, c0, 5\n" // Refuse to run on cores other than #0.
        "ands.w r8, r8, #3\n"       // read the lowest 2 bits of the MPIDR register,
        "it     ne\n"               // bail if CPU ID is nonzero (i.e. other cores)
        "bne    bail\n"
    #endif

    );

//    asm(".text\n"
//        ".thumb\n"
//        "ldr    "
//
//    );

    //bmp_printf_auto("HOOKED (on a feeling)");
    //DryosDebugMsg(0, 15, "[SJE] in hook code");
    //info_led_blink(11, 50, 50);
    //log_finish();
    //log_start();

    asm(".text\n"
        ".thumb\n"
    "bail:\n"
        // restore the overwritten instructions,
        // pop everything, jump back.
        //
        // This means our hook only triggers once.  Maybe that's good enough?
        // If not, need to rig something to keep putting the hook back.
        // The pre- or post- ISR hooks?
        // Create a task that fires after a short delay?

        //df00a434 2d e9 f0 47     push       { r4, r5, r6, r7, r8, r9, r10, lr  }
        //df00a438 88 46           mov        r8,param_2
        //df00a43a 3d 49           ldr        param_2,[PTR_PTR_s_TaskClass_df00a530]           = df00ee90
        "ldr    r8, =0xdf00a434\n" // addr in hooked func
        "ldr    r7, =0x47f0e92d\n" // saved bytes from hooked function
        "str    r7, [r8, 0]\n" // restore saved bytes into func

        "ldr    r8, =0xdf00a438\n"
        "ldr    r7, =0x493d4688\n" // more saved bytes to restore
        "str    r7, [r8, 0]\n"
        // maybe sync caches here?  Shouldn't be needed,
        // as 0xdf00a434 is in NCACH region.

//        "pop    {r0-r11}\n"
        "ldr    pc, =0xdf00a435\n"
    );

    /*
    Hook code looks like this:
        asm(
            "ldr pc, [pc, 0]\n"
            ".word 0x12345678\n"
        );

    Which is 8 bytes.
    */
}
