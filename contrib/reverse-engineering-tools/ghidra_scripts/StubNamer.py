#Loads a stubs.S, finds function stubs, disassembles and labels those addresses
#@author SJE
#@category Magiclantern
#@keybinding 
#@menupath 
#@toolbar 

def stub_namer(address, name):
    print("Disassembling '%s' at %s" % (name, address.toString()))
    disassemble(address)
    createLabel(address, name, True)


try:
    stubs_path = askFile("Select stubs.S", "OK").getAbsolutePath()
except ghidra.util.exception.CancelledException:
    exit()

# The following may not work on Windows, we may be opening the file twice.
stubs = [] # will be list of (address, name) tuples
bad_addresses = [] # stub lines we didn't understand
with open(stubs_path, "r") as f:
    for line in f.readlines():
        # assume functions live in the 0x10000000-0xffffffff region, eg
        # NSTUB(0xFF86DD88,  take_semaphore)
        if line.startswith("NSTUB("):
            address = line.split(",")[0].split("(")[1].strip()
            name = line.split(",")[1].split(")")[0].strip()
            try:
                address_int = int(address, 16) & ~1
            except ValueError:
                bad_addresses.append(address)
                continue
            if address != "ROMBASEADDR" and address_int != 0xdeadbeef:
                if address_int >= 0x10000000 and address_int < 0xffffffff:
                    stubs.append((toAddr(address), name))
            else:
                pass #TODO determine ROMBASEADDR and handle it

for s in stubs:
    stub_namer(s[0], s[1])

if bad_addresses:
    print("Some stubs weren't understood:")
    for a in bad_addresses:
        print(a)
